package com.computacion.permisosusb;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/*
 * Fragment que muestra el permiso para la solicitud de extraplanes. 
 */
public class Permiso_Extraplanes extends Fragment {

	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
			   Bundle savedInstanceState) {
			  return inflater.inflate(R.layout.permiso_extraplan, container,false);
			 }

	
}
