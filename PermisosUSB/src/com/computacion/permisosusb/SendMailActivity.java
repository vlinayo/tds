package com.computacion.permisosusb;

import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/*
 * SendMailActivity
 * 
 * Clase que implementa todos aquellos metodos necesarios
 * para la implementcion de la creacion del mail, donde
 * se configuran los atributos necesarios para el email
 * 
 */
public class SendMailActivity extends Activity {

	final Context context = this;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		enviarCorreo();
	}

	public void enviarCorreo(){

		MyApplication state = (MyApplication)getApplicationContext();
		String fileName     = state.getStudentId() + ".txt";
		Log.i("SendMailActivity", "Send Button Clicked.");
		String fromEmail = state.getStudentMail();
		String fromPassword = state.getContrasenaCorreo();
		String toEmails = "vlinayo@gmail.com";//"coord-comp@usb.ve";
		@SuppressWarnings("rawtypes")
		List toEmailList = Arrays.asList(toEmails
				.split("\\s*,\\s*"));
		Log.i("SendMailActivity", "To List: " + toEmailList);
		String nombre = state.getStudentName() ;
		String emailSubject = "Solicitud de Permiso " + nombre;
		String emailBody = state.getSolicitud();

		realizarConexion(fromEmail,
				fromPassword, toEmailList, toEmails,emailSubject, emailBody,fileName);

	}


//	@SuppressLint("InflateParams") @SuppressWarnings("unchecked")
	public void realizarConexion(String fromEmail,
			String fromPassword, @SuppressWarnings("rawtypes") List toEmailList,
			String toEmails,String emailSubject,String emailBody,String fileName){


//		SendMailTask mailTask = new SendMailTask(SendMailActivity.this);

//		if(mailTask.probarConexion(fromEmail,fromPassword)){
			SendMailTask mailTask2 = new SendMailTask(SendMailActivity.this);
			mailTask2.execute(fromEmail,
					fromPassword, toEmailList, toEmails,emailSubject, emailBody,fileName);
			
			//vista exitosa.
			setContentView(R.layout.envio_exitoso);
//			//										startActivity(new Intent(SendMailActivity.this, EnvioExitoso.class));			
//		}else{
//
//			LayoutInflater layoutInflater = LayoutInflater.from(context);
//
//			View promptView = layoutInflater.inflate(R.layout.prompts, null);
//
//			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//
//			// set prompts.xml to be the layout file of the alertdialog builder
//			alertDialogBuilder.setView(promptView);
//			final TextView msj  =(TextView) promptView.findViewById(R.id.tvMsj);
//			final EditText input = (EditText) promptView.findViewById(R.id.userInput);
//			msj.setText("Contrase�a incorrecta o conexi�n fallida:\n\n Ingrese la contrase�a nuevamente, correspondiente a su correo universitario usb.ve :");
//
//			// setup a dialog window
//			alertDialogBuilder
//			.setCancelable(false)
//			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog, int id) {
//					MyApplication state =((MyApplication) getApplicationContext());
//					state.setContrasenaCorreo(input.getText().toString());
//					String fileName     = state.getStudentId() + ".txt";
//					Log.i("SendMailActivity", "Send Button Clicked.");
//					String fromEmail = state.getStudentMail();
//					String fromPassword = state.getContrasenaCorreo();
//					String toEmails = "aileengmp@gmail.com";//"coord-comp@usb.ve";
//					@SuppressWarnings("rawtypes")
//					List toEmailList = Arrays.asList(toEmails
//							.split("\\s*,\\s*"));
//					Log.i("SendMailActivity", "To List: " + toEmailList);
//					String nombre = state.getStudentName() ;
//					String emailSubject = "Solicitud de Permiso " + nombre;
//					String emailBody = state.getSolicitud();
//					SendMailTask mailTask = new SendMailTask(SendMailActivity.this);
//
//					if(mailTask.probarConexion(fromEmail,fromPassword)){
//						SendMailTask mailTask2 = new SendMailTask(SendMailActivity.this);
//						mailTask2.execute(fromEmail,
//								input.getText().toString(), toEmailList, toEmails,emailSubject, emailBody,fileName);
//
//						//vista exitosa.
//						setContentView(R.layout.envio_exitoso);
//						//										startActivity(new Intent(SendMailActivity.this, EnvioExitoso.class));
//
//
//					}else{
//						setContentView(R.layout.envio_fallido);	
//						//vista fallida.
//					}	
//				}
//			})
//			.setNegativeButton("Cancel",
//					new DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog,	int id) {
//					dialog.cancel();
//				}
//			});
//
//			// create an alert dialog
//			AlertDialog alertD = alertDialogBuilder.create();
//
//			alertD.show();	
//
//		}
	}

	//bot�n de regresar de la vista env�o fallido.
	public void volverBtn(View v){
		Intent nextActivity = new Intent(this,Solicitud_Permisos.class);
        //Para que las vistas no sean empiladas, y el bot�n de back pueda cerrar la app.
        nextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        nextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);                  
        nextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivity(nextActivity);
	}

	//Bot�n de regresar de la vista envio con �xito.
	public void volverBtnExito(View v){
		Intent nextActivity = new Intent(this,Solicitud_Permisos.class);
        //Para que las vistas no sean empiladas, y el bot�n de back pueda cerrar la app.
        nextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        nextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);                  
        nextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivity(nextActivity);
	}

    /*
     * M�todo a ejecutar cuando se selecciona el boton "back" de android.
     * Se imprime un mensaje de confirmaci�n, y si en 3 seg el bot�n vuelve
     * a ser presionado, se sale de la aplicaci�n.  
     */
    private Boolean exit = false;
    @Override
        public void onBackPressed() {
            if (exit)
                finish();
            else {
                Toast.makeText(this, "Presione de nuevo para salir.",
                        Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);

            }

        }
	
}
