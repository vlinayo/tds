package com.computacion.permisosusb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends Activity {

	/*
	 * Actividad donde se ejecuta el SplashScreen.
	 */
	
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
 
        new Handler().postDelayed(new Runnable() {
 
        	/*
        	 * Al finalizar el timer, se ejecuta �ste m�todo el cual hace la llamada a
        	 * la siguiente actividad "StudentData"
        	 */
            @Override
            public void run() {
                // 
                // Start your app main activity
                Intent i = new Intent(MainActivity.this, StudentData.class);
                startActivity(i);
 
                // Cierra el Splash
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	finish();
    }
 
}
