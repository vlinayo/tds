package com.computacion.permisosusb;

import java.util.ArrayList;

import android.app.Application;

/*
 * Clase que implementa las estructuras que mediante
 * la extension de Application hacen posible el uso 
 * de las mismas, desde cualquier activity, en todo
 * momento
 */
public class MyApplication extends Application
{
	private String[] studentData = new String[9];
	private String graphType = ""; // Pasantias, Tesis o Exclusivo
	private String solicitud = "";// String para almacenar en el archivo
	private ArrayList<String>[] cursando = new ArrayList[6];  // Codigo de materias que esta cursando por a�o
	private ArrayList<String>[] permiso  = new ArrayList[6];   //Codigo de materias que esta solicitando para prox trimestre
	//  ------  TIPOS DE PERMISOS -----
	private ArrayList<String> electivaLibre = new ArrayList();   //Codigo de materias electiva libres a solicitar
	private ArrayList<String> electivaArea  = new ArrayList();   //Codigo de materias electiva area a solicitar
	private ArrayList<String> etapaProyecto = new ArrayList();   //Codigo de materias electiva area a solicitar
	private String maxCreditos = new String(); // permiso para maximo de creditos
	private String minCreditos = new String(); // permiso para minimo de creditos
	private String permisoOtros= new String(); // almacena anotaciones sobre el permiso en general
	private String dosGenerales= new String(); // almacena trimestre en que realiza permiso
	private String pasantiaLarga= new String(); // almacena pasantia  Larga en que realiza permiso
	private String pasantiaCorta= new String(); // almacena pasantia  Larga en que realiza permiso
	private String pp          = new String(); // almacena periodo prueba en que realiza permiso
	private String extraplanGeneral = new String();
	private String extraplan= new String();
	private String fecha			= new String();
	private String electivaAdicional = new String();
	private String proyectoDedicacion = new String();
	private String primeraEtapa      = new String();
	private String contrasenaCorreo  = new String();
	private String sinRequisito  = new String();	
	
	/*
	 * Metodo que inicializa los ArrayList de materias
	 * cursando y para permisos
	 */
	public void inicializar(){
		
		for(int i=0;i<6;i++){
			cursando[i] = new ArrayList<String>(); //initialize each individually
			permiso[i]  = new ArrayList<String>();
		}
		maxCreditos   = "no";
		minCreditos   = "no";
		permisoOtros  = "no";
		dosGenerales  = "no";
		pasantiaLarga = "no";
		pasantiaCorta = "no";
		pp			  = "no";
		extraplanGeneral = "no";
		extraplan        = "no";
		primeraEtapa     = "no";
		electivaAdicional= "no";	
		proyectoDedicacion = "no";
		sinRequisito	   = "no";
	}
	
	// ---------- CONTRASENA CORREO  SETTER ------------ //
	
	public void setContrasenaCorreo(String p){
		this.contrasenaCorreo = p;
	}
	// ---------- CONTRASENA CORREO GETTER ------------ //
	
	public String getContrasenaCorreo(){
		return this.contrasenaCorreo;
	}	

	// ------  STUDENT DATA SETTERS  --------- //
	
	public void setStudentName( String name){
		studentData[0]= name;
	}
	
	public void setStudentLastName(String lastName){
		studentData[1] = lastName;
	}
	
	public void setStudentMail(String mail){
		studentData[2] = mail;
	}	
	
	public void setStudentId(String id){
		studentData[3] = id;
	}
	
	public void setStudentPhone(String phone){
		studentData[4] = phone;
	}
	
	public void setStudentGrade(String grade){
		studentData[5] = grade;
	}
	
	public void setStudentCredits(String credits){
		studentData[6] = credits;
	}
	
	public void setStudentTrim(String trim){
		studentData[7] = trim;
	}
	
	public void setStudentTrimUltimo(String trim){
		studentData[8] = trim;
	}
		
	
	// --------  STUDENT DATA GETTERS --------- //
	
	public String getStudentName(){
		return studentData[0];
	}
	
	public String getStudentLastName(){
		return studentData[1];
	}
	
	public String getStudentMail(){
		return studentData[2];
	}	
	
	public String getStudentId(){
		return studentData[3];
	}
	
	public String getStudentPhone(){
		return studentData[4];
	}
	
	public String getStudentGrade(){
		return studentData[5];
	}
	
	public String getStudentCredits(){
		return studentData[6];
	}
	
	public String getStudentTrim(){
		return studentData[7];
	}
	
	public String getStudentTrimUltimo(){
		return studentData[8];
	}
	
	// ---------- GRAPH TYPE SETTER ------------ //
	
	public void setGraphType(String graphType){
		this.graphType = graphType;
	}
	
	// ---------- GRAPH TYPE GETTER ------------ //	
	
	public String getGraphType(){
		return this.graphType;
	}	
	
	// ---------- CURSANDO SETTER ------------ //
	
	public void setCursando(int pos, ArrayList<String> cursando){
		this.cursando[pos] = cursando;
	}
	
	// ---------- CURSANDO GETTER ------------ //
	
	public ArrayList<String> getCursando(int pos){
		return this.cursando[pos];
	}
		
	// ---------- PERMISO SETTER ------------ //
	
	public void setPermiso(int pos, ArrayList<String> permiso){
		this.permiso[pos] = permiso;
	}
	
	// ---------- PERMISO GETTER ------------ //
	
	public ArrayList<String> getPermiso(int pos){
		return this.permiso[pos];
	}
	
	// ---------- ELECTIVA LIBRE SETTER ------------ //
	
	public void setElectivaLibre(ArrayList<String> e){
		this.electivaLibre = e;
	}
	
	// ---------- ELECTIVA LIBRE GETTER ------------ //
	
	public ArrayList<String> getElectivaLibre(){
		return this.electivaLibre;
	}
			
	// ---------- ELECTIVA AREA SETTER ------------ //
	
	public void setElectivaArea(ArrayList<String> e){
		this.electivaArea = e;
	}
	
	// ---------- ELETIVA AREA GETTER ------------ //
	
	public ArrayList<String> getElectivaArea(){
		return this.electivaArea;
	}
	
	// ---------- ETAPA PROYECTO SETTER ------------ //
	
	public void setEtapaProyecto(ArrayList<String> e){
		this.etapaProyecto = e;
	}
	
	// ---------- ETAPA PROYECTO GETTER ------------ //
	
	public ArrayList<String> getEtapaProyecto(){
		return this.etapaProyecto;
	}	
	
	// ---------- MAX CREDITOS SETTER ------------ //
	
	public void setMaxCreditos(String s){
		this.maxCreditos = s;
	}
	
	// ---------- MAXIMO CREDITOS GETTER ------------ //
	
	public String getMaxCreditos(){
		return this.maxCreditos;
	}	
	
	// ---------- MINIMO CREDITOS SETTER ------------ //
	
	public void setMinCreditos(String s){
		this.minCreditos = s;
	}
	// ---------- MINIMO CREDITOS GETTER ------------ //
	
	public String getMinCreditos(){
		return this.minCreditos;
	}	
	
	// ---------- PERMISO OTROS SETTER ------------ //
	
	public void setPermisoOtros(String s){
		this.permisoOtros = s;
	}
	// ---------- PERMISO OTROS GETTER ------------ //
	
	public String getPermisoOtros(){
		return this.permisoOtros;
	}	
	
	// ---------- DOS GENERALES SETTER ------------ //
	
	public void setDosGenerales(String s){
		this.dosGenerales = s;
	}
	// ---------- DOS GENERALES GETTER ------------ //
	
	public String getDosGenerales(){
		return this.dosGenerales;
	}		
	
	// ---------- PASANTIA  LARGA SETTER ------------ //
	
	public void setPasantiaLarga(String p){
		this.pasantiaLarga = p;
	}
	// ---------- PASANTIA LARGA GETTER ------------ //
	
	public String getPasantiaLarga(){
		return this.pasantiaLarga;
	}	
	
	// ---------- PASANTIA  CORTA SETTER ------------ //
	
	public void setPasantiaCorta(String p){
		this.pasantiaCorta = p;
	}
	// ---------- PASANTIA CORTA GETTER ------------ //
	
	public String getPasantiaCorta(){
		return this.pasantiaCorta;
	}	
	
	// ---------- PERIODO PRUEBA SETTER ------------ //
	
	public void setPeriodoPrueba(String p){
		this.pp = p;
	}
	// ---------- PERIODO PRUEBA GETTER ------------ //
	
	public String getPeriodoPrueba(){
		return this.pp;
	}		

	// ---------- EXTRAPLAN GENERAL SETTER ------------ //
	
	public void setExtraplanGeneral(String p){
		this.extraplanGeneral = p;
	}
	// ---------- EXTRAPLAN GENERAL GETTER ------------ //
	
	public String getExtraplanGeneral(){
		return this.extraplanGeneral;
	}	

	// ---------- EXTRAPLAN  SETTER ------------ //
	
	public void setExtraplan(String p){
		this.extraplan = p;
	}
	// ---------- EXTRAPLAN GETTER ------------ //
	
	public String getExtraplan(){
		return this.extraplan;
	}	
	
	
	
	// ---------- SOLICITUD SETTER ------------ //
	
	public void setSolicitud(String s){
		this.solicitud = s;
	}
	
	// ---------- SOLICITUD GETTER ------------ //
	
	public String getSolicitud(){
		return this.solicitud;
	}	
	
	// ---------- FECHA SETTER ------------ //
	
	public void setFecha(String s){
		this.fecha = s;
	}
	
	// ---------- FECHA GETTER ------------ //
	
	public String getFecha(){
		return this.fecha;
	}	
	
	// ---------- SIN REQUISITO SETTER ------------ //
	public void setSinRequisito(String s){
		this.sinRequisito = s;
	}
	
	// ---------- SIN REQUISITO GETTER ------------ //
	
	public String getSinRequisito(){
		return this.sinRequisito;
	}		
	
	// ---------- PROYECTO A DEDICACI�N SETTER ------------ //
	
	public void setProyectoDedicacion(String p){
		this.proyectoDedicacion = p;
	}
	// ---------- PROYECTO A DEDICACI�N GETTER ------------ //
	
	public String getProyectoDedicacion(){
		return this.proyectoDedicacion;
	}	
	
	// ---------- ELECTIVA O GENERAL ADICIONAL SETTER ------------ //
	
	public void setElectivaAdicional(String p){
		this.electivaAdicional = p;
	}
	// ---------- ELECTIVA O GENERAL ADICIONAL GETTER ------------ //
	
	public String getElectivaAdicional(){
		return this.electivaAdicional;
	}	
	
	// ---------- PRIMERA ETAPA DE PROYECTO + TOPICO I SETTER ------------ //
	
	public void setPrimeraEtapa(String s){
		this.primeraEtapa = s;
	}
	
	// ---------- PRIMERA ETAPA DE PROYECTO + TOPICO I GETTER ------------ //
	
	public String getPrimeraEtapa(){
		return this.primeraEtapa;
		
	}
}