package com.computacion.permisosusb;

import java.util.Calendar;
import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


public class StudentData extends ActionBarActivity {

	 private EditText  username=null;
	 private EditText  mail=null;
	 private EditText id = null;
	 private EditText phone = null;
	 private EditText grade = null;
	 private EditText credits = null;
	 private CheckBox savelogin;
	 private SharedPreferences loginPreferences;
	 private SharedPreferences.Editor loginPrefsEditor;	
	 private Boolean saveLogin;
	 private Spinner trim;
	 private Spinner trimUltimo;
	 
	 /*
	  * Patrones de expresiones regulares para validaci�n de los campos.
	  */
	 public final Pattern EMAIL_ADDRESS_PATTERN = Pattern
	            .compile("[0-9]{2}\\-[0-9]{5}" + "@usb.ve");
	 public final Pattern USER_PATTERN = Pattern
			 .compile("[a-zA-Z�������\\-']+ [a-zA-Z�������\\-']+");
	 public final Pattern ID_PATTERN = Pattern
			 .compile("[0-9]{2}\\-[0-9]{5}");
	 public final Pattern PHONE_PATTERN = Pattern
			 .compile("[0-9]{4}[\\.\\-]?[0-9]{7}");
 
	 
	 /*
	  * M�todo onCreate, donde se hacen las llamadas y asignaciones necesarias para levantar la vista que recolecta
	  * los datos del usuario.
	  */
	
	@Override
	    protected void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
			setContentView(R.layout.student_data);
			
			/*
			 * Obtenemos los valores que registra el usuario.
			 */
		    username = (EditText)findViewById(R.id.user_name);
		    id = (EditText)findViewById(R.id.user_id);
		    mail = (EditText)findViewById(R.id.user_mail);
		    mail.setOnFocusChangeListener(new OnFocusChangeListener(){
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if(hasFocus){
					    mail.setText(getMail()+"");
					}else{
						//do nothing.
					}
				}
		    });
		    phone = (EditText)findViewById(R.id.user_phone);
		    grade = (EditText)findViewById(R.id.indice);
		    credits = (EditText)findViewById(R.id.creditos_apb);	
		    trim = (Spinner) findViewById(R.id.trimestreRango);
		    trimUltimo = (Spinner) findViewById(R.id.trimestreUltimo);
		    savelogin = (CheckBox)findViewById(R.id.remember_user);
	        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
	        loginPrefsEditor = loginPreferences.edit();

	        saveLogin = loginPreferences.getBoolean("saveLogin", false);
	        if (saveLogin == true) {
	        	username.setText(loginPreferences.getString("username", ""));
	            mail.setText(loginPreferences.getString("mail", ""));
	            id.setText(loginPreferences.getString("id", ""));
	            phone.setText(loginPreferences.getString("phone", ""));
	            grade.setText(loginPreferences.getString("grade", ""));
	            credits.setText(loginPreferences.getString("credits", ""));
	            savelogin.setChecked(true);
	        }
	        
	        
	        
	        // Calcular fecha del telefono, que sera de la solicitud
	        Calendar c = Calendar.getInstance();
	        int mYear = c.get(Calendar.YEAR);
	        int mMonth = c.get(Calendar.MONTH);
	        int mDay = c.get(Calendar.DAY_OF_MONTH);
	        
	
	        // Almacenar los datos en la estructura general de la aplicacion
	        // para al final ser almacenado todo en un archivo
	        
	        MyApplication state = ((MyApplication) getApplicationContext());
	        String str = username.getText().toString();
	        state.setStudentName(str);
	        state.setStudentId(id.getText().toString());
	        state.setStudentPhone(phone.getText().toString());
	        state.setStudentGrade(grade.getText().toString());
	        state.setStudentMail(mail.getText().toString());
	        state.setStudentCredits(credits.getText().toString());
	       // state.setStudentTrim(trim.getSelectedItem().toString());   
	       // state.setStudentTrimUltimo(trimUltimo.getSelectedItem().toString());
	        state.setFecha(mYear+"/"+mMonth+"/"+mDay);
	        System.out.println(mYear+"/"+mMonth+"/"+mDay);

	        trim.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
	            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) { 
	    	        MyApplication state = ((MyApplication) getApplicationContext());
	            	state.setStudentTrim(trim.getSelectedItem().toString());
	            } 

	            public void onNothingSelected(AdapterView<?> adapterView) {
	    	        MyApplication state = ((MyApplication) getApplicationContext());	            	
	            	state.setStudentTrim(trim.getSelectedItem().toString());
	            } 
	        });
	        
	        
	        trimUltimo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
	            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) { 
	    	        MyApplication state = ((MyApplication) getApplicationContext());
	    	        state.setStudentTrimUltimo(trimUltimo.getSelectedItem().toString());
	            } 

	            public void onNothingSelected(AdapterView<?> adapterView) {
	    	        MyApplication state = ((MyApplication) getApplicationContext());
	    	        state.setStudentTrimUltimo(trimUltimo.getSelectedItem().toString());
	            } 
	        }); 
		 }
	        
		

	 /*
	  * Funci�n para auto-generar el correo del usuario.
	  */
		private String getMail() {
			String ID = id.getText().toString();
			String ID2 = ID.concat("@usb.ve");
		return ID2;
	}

		/*
		 * M�todo que permite verificar la correctitud del correo.
		 */
		
		private boolean checkEmail(String email) {
			if(EMAIL_ADDRESS_PATTERN.matcher(email).matches() == true){
		        return true;
			}else{
			     errorMessageEmail();
			      return false;
			}
	    }
		
		private void errorMessageEmail() {
			 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

	         dlgAlert.setMessage("Verif�que que el campo correo no est� vac�o y que es de la forma: 12-34567@usb.ve");
	         dlgAlert.setTitle("Mensaje de Error");
	         dlgAlert.setPositiveButton("OK", null);
	         dlgAlert.setCancelable(true);
	         dlgAlert.create().show();

	         dlgAlert.setPositiveButton("Ok",
	             new DialogInterface.OnClickListener() {
	                 public void onClick(DialogInterface dialog, int which) {

	             }
	         });		
		}						
		
		/*
		 * M�todo que permite verificar la correctitud del nombre del usuario.
		 */
		private boolean checkUsername(String username) {
	        if(USER_PATTERN.matcher(username).matches() == true){
		        return true;
			}else{
				errorMessageUsername();
			      return false;
			}
	    }
		
		private void errorMessageUsername() {
			 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

	         dlgAlert.setMessage("Verif�que que el campo Nombre y Apellido no est� vac�o");
	         dlgAlert.setTitle("Mensaje de Error");
	         dlgAlert.setPositiveButton("OK", null);
	         dlgAlert.setCancelable(true);
	         dlgAlert.create().show();

	         dlgAlert.setPositiveButton("Ok",
	             new DialogInterface.OnClickListener() {
	                 public void onClick(DialogInterface dialog, int which) {

	             }
	         });		
		}				

		
		/*
		 * M�todo que permite verificar la correctitud del carnet.
		 */
		private boolean checkId(String id, String email){
			String[] parts = email.split("@");;
			String parts1 = parts[0]; // 08-10615
			
			if(ID_PATTERN.matcher(id).matches() == true){   //es un carnet.
				if(parts1.equals(id)){			//si el carnet es igual al carnet del correo.
					return true;					
				}else{
					Toast.makeText(getApplicationContext(), "El carnet debe ser el mismo al carnet en el correo",
				    	      Toast.LENGTH_SHORT).show();
				      return false;
				}     
			}else{
				  errorMessageId();
			      return false;
			}
		}	
		
		private void errorMessageId() {
			 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

	         dlgAlert.setMessage("Verif�que que el campo Carnet no est� vac�o y sea de la forma: 12-34567");
	         dlgAlert.setTitle("Mensaje de Error");
	         dlgAlert.setPositiveButton("OK", null);
	         dlgAlert.setCancelable(true);
	         dlgAlert.create().show();

	         dlgAlert.setPositiveButton("Ok",
	             new DialogInterface.OnClickListener() {
	                 public void onClick(DialogInterface dialog, int which) {

	             }
	         });		
		}		
		
		/*
		 * M�todo que permite verificar la correctitud del telefono.
		 */
		private boolean checkPhone(String phone){
			if(PHONE_PATTERN.matcher(phone).matches()==true){
				return true;
			}else{
				  errorMessagePhone();
			      return false;
				
			}
		}
		
		private void errorMessagePhone() {
			 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

	         dlgAlert.setMessage("Verif�que que el campo Tel�fono no est� vac�o y sea de la forma: \n 12345678900 \n 1234.5678900  \n 1234-5678900");
	         dlgAlert.setTitle("Mensaje de Error");
	         dlgAlert.setPositiveButton("OK", null);
	         dlgAlert.setCancelable(true);
	         dlgAlert.create().show();

	         dlgAlert.setPositiveButton("Ok",
	             new DialogInterface.OnClickListener() {
	                 public void onClick(DialogInterface dialog, int which) {

	             }
	         });		
		}		

	/*
	 * M�todo que maneja lo que ocurre al hacer click en el bot�n. 
	 */
	public void contBtn(View view) {

	    		if( checkUsername(username.getText().toString()) &&
	    		checkEmail(mail.getText().toString()) &&
	    		checkId(id.getText().toString(), mail.getText().toString()) &&
	    		checkPhone(phone.getText().toString())){
	    		    	
	    	/*
	    	 * El siguiente if maneja el checkbox, y almacena los datos del usuario.
	    	 */
	           if (savelogin.isChecked()) {
	                loginPrefsEditor.putBoolean("saveLogin", true);
	                loginPrefsEditor.putString("username", username.getText().toString());
	                loginPrefsEditor.putString("mail", mail.getText().toString());
	                loginPrefsEditor.putString("id", id.getText().toString());
	                loginPrefsEditor.putString("phone", phone.getText().toString());
	                loginPrefsEditor.putString("grade", grade.getText().toString());
	                loginPrefsEditor.putString("credits", credits.getText().toString());
	                loginPrefsEditor.commit();
	            } else {
	                loginPrefsEditor.clear();
	                loginPrefsEditor.commit();
	            }
	           //llama al m�todo que cambia la vista (si cumple los requisitos).
	           changeView();

	     }//FinIf.
	}//fincontBtn.


	/*
	 * M�todo que llamamos al hacer click en el bot�n y al haber verificado los datos.
	 */
	public void changeView(){
        startActivity(new Intent(StudentData.this, GraphTypeSelection.class));
	}
}