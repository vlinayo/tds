package com.computacion.permisosusb;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
/*
 * SendMailTask
 * 
 * Clase que implementa el proceso en background
 * que hace posible el envio del mail
 * 
 */
public class SendMailTask extends AsyncTask {

	private ProgressDialog statusDialog;
	private Activity sendMailActivity;

	/*
	 * Constructor de la clase SendMailTask
	 * @param activity
	 */
	public SendMailTask(Activity activity) {
		sendMailActivity = activity;
	}

//	/*
//	 * Metodo que prepara el mensaje a ser mostrado
//	 * al enviar el correo electronico
//	 * @see android.os.AsyncTask#onPreExecute()
//	 */
//	protected void onPreExecute() {
//		statusDialog = new ProgressDialog(sendMailActivity);
//		statusDialog.setMessage("Getting ready...");
//		statusDialog.setIndeterminate(false);
//		statusDialog.setCancelable(false);
//		statusDialog.show();
//	}

	/*
	 * Metodo que implementa el envio de correos electronicos
	 * por SMTP en background
	 * @see android.os.AsyncTask#doInBackground(java.lang.Object[])
	 */
	@Override
	protected Object doInBackground(Object... args) {
		try {
			Log.i("SendMailTask", "About to instantiate GMail...");
			publishProgress("Processing input....");
			GMail androidEmail = new GMail(args[0].toString(),
					args[1].toString(), (List) args[2], args[3].toString() , args[4].toString(),
					args[5].toString(),args[6].toString());
			
			publishProgress("Preparando mensaje ....");
			androidEmail.createEmailMessage();
			publishProgress("Enviando correo ....");
			androidEmail.sendEmail();
			publishProgress("Correo enviado.");
			Log.i("SendMailTask", "Mail Sent.");
			
		} catch (Exception e) {
			publishProgress(e.getMessage());
			Log.e("SendMailTask", e.getMessage(), e);
		}
		return null;
	}
	
	
	public Boolean probarConexion(Object... args) {
		try {
			Log.i("SendMailTask", "About to instantiate GMail..probarConexion.");
			publishProgress("Processing input....");
			GMail androidEmail = new GMail(args[0].toString(),args[1].toString());
			androidEmail.probarEmail();
			return true;
		} catch (Exception e) {
			publishProgress(e.getMessage());
			Log.e("SendMailTask", e.getMessage(), e);
			return false;
		}
	}

//	/*
//	 * Metodo que actualiza el mensaje a mostrar
//	 * @see android.os.AsyncTask#onProgressUpdate(java.lang.Object[])
//	 */
//	@Override
//	public void onProgressUpdate(Object... values) {
//		statusDialog.setMessage("");
//	}
//
//	/*
//	 * Metodo que implementa lo que se realizara luego
//	 * de mostrar el mensaje
//	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
//	 */
//	@Override
//	public void onPostExecute(Object result) {
//		statusDialog.dismiss();
//	}
//	
	
}