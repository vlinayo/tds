package com.computacion.permisosusb;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/*
 * Fragment que muestra la vista para realizar el env�o de los permisos seleccionados. 
 */
public class Permiso_Envio extends Fragment{

	@Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
			   Bundle savedInstanceState) {
			  return inflater.inflate(R.layout.permiso_envio, container,false);
	}

}

	
