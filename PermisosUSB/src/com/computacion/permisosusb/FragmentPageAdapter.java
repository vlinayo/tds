package com.computacion.permisosusb;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class FragmentPageAdapter extends FragmentPagerAdapter {
	
	/*
	 * Adaptador para los tabs.
	 * Es aqu� donde se crea el adaptador que va a manejar las pesta�as, que a 
	 * su vez ser� quien haga las llamadas a los distintos Fragments de cada pesta�a.
	 */
	 public FragmentPageAdapter(FragmentManager fm) {
	  super(fm);
	 }

	 /*
	  * Obtiene la vista.
	  * Seg�n cada caso, muestra la vista indicada al usuario.
	  */
	 @Override
	 public Fragment getItem(int arg0) {
	  switch (arg0) {
	  case 0:
	      return new Permiso_Electivas_Areas();
	  case 1:
		  return new Permiso_Electivas_Libres();	  
	  case 2:
		  return new Permiso_Generales();
	  case 3:
		  return new Permiso_Extraplanes();
	  case 4:
		  return new Permiso_Sin_Requisito();
	  case 5:
		  return new Permiso_MinCreditos();	    
	  case 6:
	      return new Permiso_ExcesoCreditos();
	  case 7:
		  return new Permiso_Pasantia();		
	  case 8:
		  return new Permiso_EtapaP();  
	  case 9:
		  return new Permiso_DedicacionE();
	  case 10:
		  return new Permiso_Otros();
	  case 11:
		  return new Permiso_Envio();
	  default:
	   break;
	  }
	  return null;
	 }//fin getItem.

	 
	 /*
	  * Contador del n�mero de p�ginas a mostrar en la solicitud de permisos.
	  * �ste m�todo es quien maneja los fragments y la cantidad de fragments que se
	  * podr�n observar.
	  */
	 @Override
	 public int getCount() {
	  return 12;
	 }//Fin getCount.


	}//Fin FragmentPageAdapter.