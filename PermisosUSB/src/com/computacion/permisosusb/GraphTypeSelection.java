package com.computacion.permisosusb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class GraphTypeSelection extends ActionBarActivity {
	
	   private RadioGroup radioGroupWebsite;
	   private RadioButton radioBtn1;
	   private Button btn;
	   private RadioButton radioBtnIntern;
	   private RadioButton radioBtnThesis;
	   private RadioButton radioBtnExclusive;
	   

	   @Override
	   protected void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     setContentView(R.layout.graph_type_menu);

	     radioBtnIntern = (RadioButton)findViewById(R.id.internship);
	     radioBtnThesis = (RadioButton)findViewById(R.id.thesis);
	     radioBtnExclusive = (RadioButton)findViewById(R.id.thesis2);
	     
	     addListenerRadioButton();
	     
	     
	   }
	   
	   
	   /*
	    * M�todo encargado de manejar los Radio Buttons, en �ste m�todo
	    * se verifica que s�lo un bot�n pueda estar seleccionado a la vez
	    * y que al seleccionar uno de los botones y hacer click sobre "continuar"
	    * la vista cambie como corresponde.
	    */
	   private void addListenerRadioButton() {

	       radioGroupWebsite = (RadioGroup) findViewById(R.id.radioGroup1);
	       btn = (Button) findViewById(R.id.next);
	       btn.setOnClickListener(new OnClickListener() {
	     
	          public void onClick(View v) {

	            // Obtener el bot�n seleccionado del Grupo de botones.
	            int selected =  
	            radioGroupWebsite.getCheckedRadioButtonId();
	            radioBtn1 = (RadioButton) findViewById(selected);
	            if(radioBtn1.equals(radioBtnIntern)){	            		
	            	try {
						goToPasantias();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
	            }
	            else if(radioBtn1.equals(radioBtnThesis)){
	            	goToThesis();
	            }
	            else if(radioBtn1.equals(radioBtnExclusive)){
	            	goToExclusive();
	            }
	          }


	    	 });	
		}

/*########################Manejamos que vista asumir� cada tipo de grafo.###################*/
	   
	
	/*
	 * Manejamos la acci�n a ejecutar si se selecciona "Proyecto a Dedicaci�n Exclusiva".   
	 */
	private void goToExclusive() {
	   	try{
    		String data = "Dedicaci�n Exclusiva";
 
    		File file =new File(Environment.getExternalStorageDirectory() + "/Estudiante.txt");
 
    		//if file doesnt exists, then create it
    		if(!file.exists()){
    			file.createNewFile();
    		}
 
    		//true = append file
    		FileWriter fileWritter = new FileWriter(file,true);
    	    BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
    	    bufferWritter.write(data);
    	    bufferWritter.close();
 
	        System.out.println("Done");
 
    	}catch(IOException e){
    		e.printStackTrace();
    	}
	   	

	   	MyApplication state = ((MyApplication) getApplicationContext());
	   	state.setGraphType("Dedicacion Exclusiva");
	   	
		
        startActivity(new Intent(GraphTypeSelection.this, Exclusive_User_GraphFill01.class));
		
	}
	
	/*
	 * Manejamos la acci�n a ejecutar si se selecciona el "Proyecto de Grado".
	 */
	private void goToThesis() {
	   	try{
    		String data = "Tesis";
 
    		File file =new File(Environment.getExternalStorageDirectory() + "/Estudiante.txt");
 
    		//if file doesnt exists, then create it
    		if(!file.exists()){
    			file.createNewFile();
    		}
 
    		//true = append file
    		FileWriter fileWritter = new FileWriter(file,true);
    	    BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
    	    bufferWritter.write(data);
    	    bufferWritter.close();
 
	        System.out.println("Done");
 
    	}catch(IOException e){
    		e.printStackTrace();
    	}
	   	
	   	MyApplication state = ((MyApplication) getApplicationContext());
	   	state.setGraphType("Proyecto de Grado");
	   	
	   	
        startActivity(new Intent(GraphTypeSelection.this, Thesis_User_GraphFill01.class));
		
	}
	
	/*
	 * Manejamos la acci�n a ejecutar si se selecciona "Pasant�a"
	 */
	public void goToPasantias() throws FileNotFoundException{
	   	try{
    		String data = "Pasant�a";
 
    		File file =new File(Environment.getExternalStorageDirectory() + "/Estudiante.txt");
 
    		//if file doesnt exists, then create it
    		if(!file.exists()){
    			file.createNewFile();
    		}
 
    		//true = append file
    		FileWriter fileWritter = new FileWriter(file,true);
    	    BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
    	    bufferWritter.write(data);
    	    bufferWritter.close();
 
	        System.out.println("Done");
 
    	}catch(IOException e){
    		e.printStackTrace();
    	}

	   	MyApplication state = ((MyApplication) getApplicationContext());
	   	state.setGraphType("Pasantia");
	   	
        startActivity(new Intent(GraphTypeSelection.this, Internship_User_GraphFill01.class));
	}	
        
 }
