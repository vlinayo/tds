package com.computacion.permisosusb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

public class Internship_User_GraphFill05 extends ActionBarActivity {

	private Button b;	
	private RadioButton radioPasada ;
	private RadioButton radioCursando ;
	private RadioButton radioPermiso ;
	private CheckBox seleccionarTodo;
	private Button b0,b1,b2,b3,b4,b5;
	private ArrayList<Button> lista;
	
	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
			setContentView(R.layout.pasantia_student_graph_fill_05);
		}
	
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.main, menu);
	 
	        return super.onCreateOptionsMenu(menu);
	    }
		
	    public boolean onOptionsItemSelected(MenuItem item) {
	    	
	    	  // Se almacena/refresh datos en la estructura general
	    	
	    	  MyApplication state        = ((MyApplication) getApplicationContext());
	    	  ArrayList<String> cursando = new ArrayList();
	    	  ArrayList<String> permiso  = new ArrayList();
	    	  ArrayList<String> electivaLibre = state.getElectivaLibre();
	    	  ArrayList<String> electivaArea  = state.getElectivaArea();	    	  
	    	  	    	  
				b0 =(Button) findViewById(R.id.belectivaArea);
				b1 =(Button) findViewById(R.id.belectivaArea2);
				b2 =(Button) findViewById(R.id.belectivaLibre);
				b3 =(Button) findViewById(R.id.belectivaLibre2);
				b4 =(Button) findViewById(R.id.bestudiosGenerales1);
				b5 =(Button) findViewById(R.id.bestudiosGenerales2);
		  		
		  		lista = new ArrayList<Button>(Arrays.asList(b0,b1,b2,b3,b4,b5));
		  		Iterator i = lista.iterator();
	    	  while(i.hasNext()){
	    		  b = (Button)i.next();
	    		  if(b.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.border_button_style_cursando).getConstantState())) { 
	    			 cursando.add(b.getHint().toString());
	    		  }else if(b.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.border_button_style_permiso).getConstantState())){
	     			 permiso.add(b.getHint().toString());
	    		  }  
	    	  }
	    	  state.setCursando(4, cursando);
	    	  state.setPermiso(4, permiso);
	    	  state.setElectivaArea(electivaArea);
	    	  state.setElectivaLibre(electivaLibre);
	          Intent nextActivity = new Intent(this,Internship_User_GraphFill_Pasantia_Larga.class);
	          startActivity(nextActivity);
	       
	        return super.onOptionsItemSelected(item);
	    }
	    
		public void agregarMateria(View view) {

			radioPasada = (RadioButton) findViewById(R.id.rbMateriaPasada);
			radioCursando = (RadioButton) findViewById(R.id.rbMateriaCursando);
			radioPermiso = (RadioButton) findViewById(R.id.rbMateriaPermiso);
				
			b =(Button) findViewById(view.getId());	
			if(radioPasada.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_pasada);			
			}else if (radioCursando.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_cursando);			
			}else if(radioPermiso.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_permiso);			
			}
				
		}
		
		
		public void seleccionarTodo(View view){
			
			b0 =(Button) findViewById(R.id.belectivaArea);
			b1 =(Button) findViewById(R.id.belectivaArea2);
			b2 =(Button) findViewById(R.id.belectivaLibre);
			b3 =(Button) findViewById(R.id.belectivaLibre2);
			b4 =(Button) findViewById(R.id.bestudiosGenerales1);
			b5 =(Button) findViewById(R.id.bestudiosGenerales2);	
			
			seleccionarTodo = (CheckBox)findViewById(R.id.select_all);
			
			if(seleccionarTodo.isChecked()){
				
				b0.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b1.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b2.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b3.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b4.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b5.setBackgroundResource(R.drawable.border_button_style_pasada);	
			}else{
				b0.setBackgroundResource(R.drawable.border_button_style);		
				b1.setBackgroundResource(R.drawable.border_button_style);		
				b2.setBackgroundResource(R.drawable.border_button_style);		
				b3.setBackgroundResource(R.drawable.border_button_style);		
				b4.setBackgroundResource(R.drawable.border_button_style);		
				b5.setBackgroundResource(R.drawable.border_button_style);	
			}
		}
		
		
		

		public void seleccionarEM(View view){
			
			b0 =(Button) findViewById(R.id.belectivaArea);
			b2 =(Button) findViewById(R.id.belectivaLibre);
			b4 =(Button) findViewById(R.id.bestudiosGenerales1);		
			
			seleccionarTodo = (CheckBox)findViewById(R.id.trimester_E_M);
			
			if(seleccionarTodo.isChecked()){
						
				b0.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b2.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b4.setBackgroundResource(R.drawable.border_button_style_pasada);	
			}else{		
				b0.setBackgroundResource(R.drawable.border_button_style);		
				b2.setBackgroundResource(R.drawable.border_button_style);		
				b4.setBackgroundResource(R.drawable.border_button_style);		
			}

		}
		
		public void seleccionarAJ(View view){
			
			b1 =(Button) findViewById(R.id.belectivaArea2);
			b3 =(Button) findViewById(R.id.belectivaLibre2);;
			b5 =(Button) findViewById(R.id.bestudiosGenerales2);			
			
			seleccionarTodo = (CheckBox)findViewById(R.id.trimester_A_J);
			
			if(seleccionarTodo.isChecked()){
						
				b1.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b3.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b5.setBackgroundResource(R.drawable.border_button_style_pasada);
			}else{
				
				b1.setBackgroundResource(R.drawable.border_button_style);		
				b3.setBackgroundResource(R.drawable.border_button_style);		
				b5.setBackgroundResource(R.drawable.border_button_style);	
			}
		}	
			
		
	}
	           