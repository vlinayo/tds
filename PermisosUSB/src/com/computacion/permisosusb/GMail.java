package com.computacion.permisosusb;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import android.util.Log;


/*
 * GMail
 * 
 * Clase que implementa todos aquellos metodos necesarios
 * para la implementcion del correo gmail por medio de
 * smtp, en background
 * 
 */
public class GMail {

	final String emailPort = "587";// gmail's smtp port
	final String smtpAuth = "true";
	final String starttls = "true";
	final String emailHost = "smtp.gmail.com";
	final Multipart _multipart = new MimeMultipart(); 

	String fromEmail;
	String fromPassword;
	List toEmailList;
	String toEmail;
	String emailSubject;
	String emailBody;
	String filename;
	Properties emailProperties;
	Session mailSession;
	MimeMessage emailMessage; 
	
	/*
	 * Constructor de la clase GMail vacio
	 */
	public GMail() {

	}
	
	
	/*
	 * Constructor de la clase 
	 */
	public GMail(String fromEmail, String fromPassword) {
		this.fromEmail = fromEmail;
		this.fromPassword = fromPassword;
		emailProperties = System.getProperties();
		emailProperties.put("mail.smtp.port", emailPort);
		emailProperties.put("mail.smtp.auth", smtpAuth);
		emailProperties.put("mail.smtp.starttls.enable", starttls);
		Log.i("GMail", "Mail server properties set.");		
		mailSession = Session.getDefaultInstance(emailProperties, null);
	}
	
	/*
	 * Constructor de la clase GMail
	 * 
	 * @param fromEmail   : correo remitente
	 * @param fromPassword: contrasena del correo remitente
	 * @param toEmailList : lista de correos destinatarios
	 * @param toEmail     : correo destinatario
	 * @param emailSubject: correo destinatario
	 * @param emailBody   : correo destinatario
	 * 
	 */
	public GMail(String fromEmail, String fromPassword,List toEmailList,
		String toEmail, String emailSubject, String emailBody, String filename) {
		this.fromEmail = fromEmail;
		this.fromPassword = fromPassword;
		this.toEmailList = toEmailList;
		this.toEmail = toEmail;
		this.emailSubject = emailSubject;
		this.emailBody = emailBody;
		this.filename  = filename;
		emailProperties = System.getProperties();
		emailProperties.put("mail.smtp.port", emailPort);
		emailProperties.put("mail.smtp.auth", smtpAuth);
		emailProperties.put("mail.smtp.starttls.enable", starttls);
		Log.i("GMail", "Mail server properties set.");
		mailSession = Session.getDefaultInstance(emailProperties, null);
		
	}

	/* createEmailMessage
	 * 
	 * Metodo encargado de implementar el "mensaje" que es
	 * enviado a traves del email a crear
	 * 
	 */
	public MimeMessage createEmailMessage() throws AddressException,
			MessagingException, UnsupportedEncodingException {

		
		
		emailMessage = new MimeMessage(mailSession);
		emailMessage.setFrom(new InternetAddress(fromEmail, fromEmail));
		Log.i("GMail","toEmail: "+toEmail);
		emailMessage.addRecipient(Message.RecipientType.TO,new InternetAddress(toEmail));
		emailMessage.setSubject(emailSubject);
		emailMessage.setContent(emailBody, "text/html");
		emailMessage.setFileName(filename);
		Log.i("GMail", "Email Message created.");
		return emailMessage;
	}


	/*
	 * sendEmail
	 * 
	 * Metodo que es implementado para realizar el envio del
	 * email, una vez que fue creado
	 * 
	 */
	public void sendEmail() throws AddressException, MessagingException {
		try{
			Transport transport = mailSession.getTransport("smtp");
			transport.connect(emailHost, fromEmail, fromPassword);
			Log.i("GMail","allrecipients: "+emailMessage.getAllRecipients());
			transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
			transport.close();
			Log.i("GMail", "Email sent successfully.");
		}catch(Exception e){
		    e.getMessage();
		 }
		
	}
	
	public void probarEmail() throws Exception {
		try{
			Transport transport = mailSession.getTransport("smtp");
			transport.connect(emailHost, fromEmail, fromPassword);
			Log.i("GMail","allrecipients: "+emailMessage.getAllRecipients());
			transport.close();
			Log.i("GMail", "Email sent successfully.");

		}catch(Exception e){
		    e.getMessage();
		 }
		
	}
	
}