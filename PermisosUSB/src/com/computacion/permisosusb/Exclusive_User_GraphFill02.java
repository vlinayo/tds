package com.computacion.permisosusb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

public class Exclusive_User_GraphFill02 extends ActionBarActivity {

	private Button b;	
	private RadioButton radioPasada ;
	private RadioButton radioCursando ;
	private RadioButton radioPermiso ;
	private CheckBox seleccionarTodo;
	private Button b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11;
	private ArrayList<Button> lista;
	
	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
			setContentView(R.layout.exclusiva_student_graph_fill_02);
		}
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.main, menu);
	 
	        return super.onCreateOptionsMenu(menu);
	    }
		
	    public boolean onOptionsItemSelected(MenuItem item) {
	    	
	    	  MyApplication state        = ((MyApplication) getApplicationContext());
	    	  ArrayList<String> cursando = new ArrayList();
	    	  ArrayList<String> permiso  = new ArrayList();
	    	  
	    	  
				b0 =(Button) findViewById(R.id.bEstructurasDiscretasUno);
				b1 =(Button) findViewById(R.id.bEstructurasDiscretasDos);
				b2 =(Button) findViewById(R.id.bEstructurasDiscretasTres);
				b3 =(Button) findViewById(R.id.bLogicaSimbolica);
				b4 =(Button) findViewById(R.id.bAlgoritmosEstructurasUno);
				b5 =(Button) findViewById(R.id.bAlgoritmosEstructurasDos);
				b6 =(Button) findViewById(R.id.bLabAlgoritmosEstructurasUno);
				b7 =(Button) findViewById(R.id.bLabAlgoritmosEstructurasDos);
				b8 =(Button) findViewById(R.id.bMatematicasIV);
				b9 =(Button) findViewById(R.id.bMatematicasV);
				b10 =(Button) findViewById(R.id.bcalculoNumerico);
				b11 =(Button) findViewById(R.id.bEstudiosGenerales);	
		  		
		  		lista = new ArrayList<Button>(Arrays.asList(b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11));
		  		Iterator i = lista.iterator();
	    	  while(i.hasNext()){
	    		  b = (Button)i.next();
	    		  if(b.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.border_button_style_cursando).getConstantState())) { 
	    			 cursando.add(b.getHint().toString());
	    		  }else if(b.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.border_button_style_permiso).getConstantState())){
	     			 permiso.add(b.getHint().toString());	
	    		  }  
	    	  }
	    	  
	    	  state.setCursando(1, cursando);
	    	  state.setPermiso(1, permiso);
	    	
	          Intent nextActivity = new Intent(this,Exclusive_User_GraphFill03.class);
	          startActivity(nextActivity);
	       
	        return super.onOptionsItemSelected(item);
	    }
		
		public void agregarMateria(View view) {

			radioPasada = (RadioButton) findViewById(R.id.rbMateriaPasada);
			radioCursando = (RadioButton) findViewById(R.id.rbMateriaCursando);
			radioPermiso = (RadioButton) findViewById(R.id.rbMateriaPermiso);
				
			b =(Button) findViewById(view.getId());	
			if(radioPasada.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_pasada);			
			}else if (radioCursando.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_cursando);			
			}else if(radioPermiso.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_permiso);			
			}
				
		}
		
		
		public void seleccionarTodo(View view){
			
			b0 =(Button) findViewById(R.id.bEstructurasDiscretasUno);
			b1 =(Button) findViewById(R.id.bEstructurasDiscretasDos);
			b2 =(Button) findViewById(R.id.bEstructurasDiscretasTres);
			b3 =(Button) findViewById(R.id.bLogicaSimbolica);
			b4 =(Button) findViewById(R.id.bAlgoritmosEstructurasUno);
			b5 =(Button) findViewById(R.id.bAlgoritmosEstructurasDos);
			b6 =(Button) findViewById(R.id.bLabAlgoritmosEstructurasUno);
			b7 =(Button) findViewById(R.id.bLabAlgoritmosEstructurasDos);
			b8 =(Button) findViewById(R.id.bMatematicasIV);
			b9 =(Button) findViewById(R.id.bMatematicasV);
			b10 =(Button) findViewById(R.id.bcalculoNumerico);
			b11 =(Button) findViewById(R.id.bEstudiosGenerales);	
			
			seleccionarTodo = (CheckBox)findViewById(R.id.select_all);
			
			if(seleccionarTodo.isChecked()){
				
				b0.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b1.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b2.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b3.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b4.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b5.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b6.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b7.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b8.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b9.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b10.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b11.setBackgroundResource(R.drawable.border_button_style_pasada);
			}else{
				b0.setBackgroundResource(R.drawable.border_button_style);		
				b1.setBackgroundResource(R.drawable.border_button_style);		
				b2.setBackgroundResource(R.drawable.border_button_style);		
				b3.setBackgroundResource(R.drawable.border_button_style);		
				b4.setBackgroundResource(R.drawable.border_button_style);		
				b5.setBackgroundResource(R.drawable.border_button_style);		
				b6.setBackgroundResource(R.drawable.border_button_style);		
				b7.setBackgroundResource(R.drawable.border_button_style);		
				b8.setBackgroundResource(R.drawable.border_button_style);		
				b9.setBackgroundResource(R.drawable.border_button_style);		
				b10.setBackgroundResource(R.drawable.border_button_style);		
				b11.setBackgroundResource(R.drawable.border_button_style);	
			}
		}
		
		
		
		public void seleccionarSD(View view){
			
			b0 =(Button) findViewById(R.id.bEstructurasDiscretasUno);;
			b3 =(Button) findViewById(R.id.bLogicaSimbolica);
			b8 =(Button) findViewById(R.id.bMatematicasIV);
			b11 =(Button) findViewById(R.id.bEstudiosGenerales);
			seleccionarTodo = (CheckBox)findViewById(R.id.trimester_S_D);
			
			if(seleccionarTodo.isChecked()){
				
				b0.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b3.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b8.setBackgroundResource(R.drawable.border_button_style_pasada);
				b11.setBackgroundResource(R.drawable.border_button_style_pasada);				
			}else{
				b0.setBackgroundResource(R.drawable.border_button_style);	
				b3.setBackgroundResource(R.drawable.border_button_style);		
				b8.setBackgroundResource(R.drawable.border_button_style);			
				b11.setBackgroundResource(R.drawable.border_button_style);				
			}
		}
		
		
		public void seleccionarEM(View view){
			
			b1 =(Button) findViewById(R.id.bEstructurasDiscretasDos);
			b4 =(Button) findViewById(R.id.bAlgoritmosEstructurasUno);
			b6 =(Button) findViewById(R.id.bLabAlgoritmosEstructurasUno);
			b9 =(Button) findViewById(R.id.bMatematicasV);
			
			seleccionarTodo = (CheckBox)findViewById(R.id.trimester_E_M);
			
			if(seleccionarTodo.isChecked()){
						
				b1.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b4.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b6.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b9.setBackgroundResource(R.drawable.border_button_style_pasada);
			}else{		
				b1.setBackgroundResource(R.drawable.border_button_style);		
				b4.setBackgroundResource(R.drawable.border_button_style);		
				b6.setBackgroundResource(R.drawable.border_button_style);	
				b9.setBackgroundResource(R.drawable.border_button_style);		
			}

		}
		
		public void seleccionarAJ(View view){
			
			b2 =(Button) findViewById(R.id.bEstructurasDiscretasTres);
			b5 =(Button) findViewById(R.id.bAlgoritmosEstructurasDos);
			b7 =(Button) findViewById(R.id.bLabAlgoritmosEstructurasDos);
			b10 =(Button) findViewById(R.id.bcalculoNumerico);
			
			seleccionarTodo = (CheckBox)findViewById(R.id.trimester_A_J);
			
			if(seleccionarTodo.isChecked()){
						
				b2.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b5.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b7.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b10.setBackgroundResource(R.drawable.border_button_style_pasada);
			}else{
				
				b2.setBackgroundResource(R.drawable.border_button_style);		
				b5.setBackgroundResource(R.drawable.border_button_style);		
				b7.setBackgroundResource(R.drawable.border_button_style);		
				b10.setBackgroundResource(R.drawable.border_button_style);	
			}
		}	
			
		
	}
