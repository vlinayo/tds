package com.computacion.permisosusb;

import java.util.ArrayList;
import java.util.Iterator;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class Solicitud_Permisos extends FragmentActivity implements ActionBar.TabListener{
	
	
	 ActionBar actionbar;
	 ViewPager viewpager;
	 FragmentPageAdapter ft;
	 final Context context = this;
	 
	 
	 @Override
	 protected void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 setContentView(R.layout.activity_solicitudpermisos);
		 	  
	  /* Se manejan los fragments, haciendo Solicitud_Permisos la vista ra�z. */
	  viewpager = (ViewPager) findViewById(R.id.pager);
	  ft = new FragmentPageAdapter(getSupportFragmentManager());
	  actionbar = getActionBar();
	  viewpager.setAdapter(ft);
	  
	  /* Se crea la barra superior y se agregan los diferentes campos seg�n los fragments que se van a usar. */
	  actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	  actionbar.addTab(actionbar.newTab().setText("�reas").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("Libres").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("Generales").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("Extraplanes").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("Sin Requisito").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("M�nimo Cr�ditos").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("M�ximo Cr�ditos").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("Pasant�a").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("Proyecto Grado").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("Proy. Dedicaci�n").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("Otros").setTabListener(this));
	  actionbar.addTab(actionbar.newTab().setText("Enviar").setTabListener(this));
	  
	  /* Se maneja la acci�n a realizar cuando se hace scroll, se indica la p�gina por defecto. */
	  viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
	      @Override
	   public void onPageSelected(int arg0) {
	   actionbar.setSelectedNavigationItem(arg0);
	       }
	      @Override
	   public void onPageScrolled(int arg0, float arg1, int arg2) {
	       }
	      @Override
	   public void onPageScrollStateChanged(int arg0) {
	       }
	  });
	  	  
	 }
	 @Override
	 public void onTabReselected(Tab tab, FragmentTransaction ft) {
	   }
	 
	 @Override
	 public void onTabSelected(Tab tab, FragmentTransaction ft) {
	  viewpager.setCurrentItem(tab.getPosition());
	   }
	 
	 @Override
	 public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		 
	 }
	 
	 
	 
	 
/* ############################## ESCRIBIR ARCHIVOS ###########################*/	  
		
		  /*
		   * cargarMateriasPermisos
		   * 
		   * Metodo que cargar los codigos de materias que el alumno
		   * esta solicitando para el proximo trimestre y devuelve
		   * un string que en la union de dichos codigos 
		   * 
		   */
		   public String cargarMateriasPermisos(){
			   
			   MyApplication state       = (MyApplication) getApplicationContext();
			   String			 permiso = new String();
			   ArrayList<String> auxPermiso = new ArrayList<String>();
			   Iterator 	     i 		 ;
			   int year = 0;
			   int c   = 0;
			   
			   while(year<6){
				   auxPermiso = state.getPermiso(year);
				   i   = auxPermiso.iterator();
				   while(i.hasNext()){
					   String p = (String)i.next();
					   if(p!=null){
						   
							   if(c==0)
								   if (!p.equals("Area") && !p.equals("LibreCuatro") && !p.equals("LibreTres") && !p.equals(" "))
								   permiso = p;
							   else
								   if (!p.equals("Area") && !p.equals("LibreCuatro") && !p.equals("LibreTres") && !p.equals(" "))
								   permiso += ", " +p;
						   
					   }
					   c++;
				   }
				   year++;
			   }
			   if (c==0 || permiso.isEmpty()){
				   permiso = "no";
			   }
			   return permiso;
		   }
		   
		  /*
		   * cargarMateriasCursando
		   * 
		   * Metodo que cargar los codigos de materias que el alumno
		   * esta cursando para el trimestre actual y devuelve
		   * un string que en la union de dichos codigos 
		   * 
		   */
		   public String cargarMateriasCursando(){
			   
			   MyApplication state        = (MyApplication) getApplicationContext();
			   String			 cursando = new String();
			   ArrayList<String> auxCursando = new ArrayList<String>();
			   Iterator 	     i 		 ;
			   int year = 0;
			   int c   = 0;
			   
			   while(year<6){
				   auxCursando = state.getCursando(year);
				   i   = auxCursando.iterator();
				   while(i.hasNext()){
					   String p = (String)i.next();
					   if(p!=null){
						   if(c==0)
							   cursando = p;
						   else
							   cursando += ", " +p;
					   }
					   c++;
				   }
				   year++;
			   }
			   if (c==0){
				   cursando = "no";
			   }			   
			   return cursando;
		   }	  
		   
		  /*
		   * cargarMateriasElectivasLibres
		   * 
		   * Metodo que cargar los codigos de materias que el alumno
		   * esta solicitando para el proximo trimestre, solo aquellas
		   * que son electivas libres y devuelve
		   * un string que en la union de dichos codigos 
		   * 
		   */
		   public String cargarMateriasElectivasLibres(){
			   
			   MyApplication     state    = (MyApplication) getApplicationContext();
			   String			 libres   = new String();
			   ArrayList<String> auxLibre = new ArrayList<String>();
			   Iterator 	     i 		  ;
			   int c    = 0;
			   auxLibre = state.getElectivaLibre();
			   i        = auxLibre.iterator();
			   while(i.hasNext()){
				   String l = (String)i.next();
				   if(l!=null){
					   if(c==0)
						   libres = l;
					   else
						   libres += ", " +l;
				   }
				   c++;
			   }
			   if (c==0){
				   libres = "no";
			   }
			   return libres;
		   }	   
		   
		  /*
		   * cargarMateriasElectivasArea
		   * 
		   * Metodo que cargar los codigos de materias que el alumno
		   * esta solicitando para el proximo trimestre , solo aquellas
		   * que son electivas de area y devuelve
		   * un string que en la union de dichos codigos 
		   * 
		   */
		   public String cargarMateriasElectivasArea(){
			   
			   MyApplication state        = (MyApplication) getApplicationContext();
			   String			 area     = new String();
			   ArrayList<String> auxArea  = new ArrayList<String>();
			   Iterator 	     i 		  ;
			   int c    = 0;
			   auxArea  = state.getElectivaArea();
			   i        = auxArea.iterator();
			   while(i.hasNext()){
				   String a = (String)i.next();
				   if(a!=null){
					   if(c==0)
						   area = a;
					   else
						   area += ", " +a;
				   }
				   c++;
			   }
			   if (c==0){
				   area = "no";
			   }			   
			   return area;
		   }	   
		   
		   
			  /*
			   * cargarMateriasEtapaProyecto
			   * 
			   * Metodo que cargar los codigos de materias que el alumno
			   * esta solicitando para el proximo trimestre , solo aquellas
			   * que son de una etapa del proyecto
			   * En algunos casos se inscriben las ultimas dos etapas
			   * 
			   */
			   public String cargarMateriasEtapaProyecto(){
				   
				   MyApplication state        = (MyApplication) getApplicationContext();
				   String			 etapa    = new String();
				   ArrayList<String> auxEtapa = new ArrayList<String>();
				   Iterator 	     i 		  ;
				   int c    = 0;
				   auxEtapa  = state.getEtapaProyecto();
				   i        = auxEtapa.iterator();
				   while(i.hasNext()){
					   String a = (String)i.next();
					   if(a!=null){
						   if(c==0)
							   etapa = a;
						   else
							   etapa += " ," +a;
					   }
					   c++;
				   }
				   if (c==0){
					   etapa = "no";
				   }			   
				   return etapa;
			   }			   
		   
		  /*
		   * escribirArchivo
		   * 
		   * Metodo que cargar los codigos de materias que el alumno
		   * esta solicitando para el proximo trimestre y devuelve
		   * un string que en la union de dichos codigos 
		   * 
		   */
		   public void escribirArchivo(){
			   
			  try{ 
		           MyApplication state            = (MyApplication) getApplicationContext();
		           String permisos                = cargarMateriasPermisos();
		           String cursando                = cargarMateriasCursando();
		           String electivasLibres         = cargarMateriasElectivasLibres();
		           String electivasArea           = cargarMateriasElectivasArea();
		           String etapaProyecto           = cargarMateriasEtapaProyecto();
		           String solicitud 			  = new String();
		           
		           // 1- Trimestre
			  	   solicitud = state.getStudentTrim();	
			  	   solicitud += "\n";
			  	   // 2 -Tipo Grafo
			  	   solicitud += state.getGraphType();
			  	   solicitud += "\n";	
			  	   // 3 -# Permisos
		           solicitud += "16";
			  	   solicitud +="\n";	
			  	   // 4 -Carnet
			  	   solicitud += state.getStudentId();//Carnet
			  	   solicitud +="\n";
			  	   // 5 -Nombre
			  	   solicitud += state.getStudentName();
			  	   solicitud += "\n";
			  	   // 6 -Promedio
			  	   solicitud += state.getStudentGrade();
			  	   solicitud += "\n";
			  	   // 7 -Telefono
			  	   solicitud += state.getStudentPhone();
			  	   solicitud += "\n";
			  	   // 8 -#CreditosAprobados
			  	   solicitud += state.getStudentCredits();
			  	   solicitud += "\n";	
			  	   // 9 -Fecha de la solicitud
			  	   solicitud += state.getFecha();
			  	   solicitud += "\n";	
			  	   // 10 -Trimestre (Antepenultimo,Ultimo, Penultimo, pp, otro)
			  	   solicitud += state.getStudentTrimUltimo();
			  	   solicitud += "\n";				  	   
		           // 11 -Dos Generales
				   solicitud += state.getDosGenerales();
			  	   solicitud += "\n";
			  	   // 12 -Extraplan y General
			  	   solicitud += state.getExtraplanGeneral();
			  	   solicitud += "\n";	
			  	   // 13 -Extraplanes
			  	   solicitud += state.getExtraplan();
			  	   solicitud += "\n";		  	   
		           // 14 -Electivas de Area
				   solicitud += electivasArea;
			  	   solicitud += "\n";	
		           // 15 -MINIMO DE CREDITOS
				   solicitud += state.getMinCreditos();	
			  	   solicitud += "\n";
		           // 16 -MAXIMO DE CREDITOS
			  	   solicitud += state.getMaxCreditos();
			  	   solicitud += "\n";
		           // 17 -Electivas Libres
				   solicitud += electivasLibres;
			  	   solicitud += "\n";			  	   
		           // 18 -Pasantia Larga
			  	   solicitud += state.getPasantiaLarga();
			  	   solicitud += "\n";
		           // 19 -Pasantia Corta
			  	   solicitud += state.getPasantiaCorta();
			  	   solicitud += "\n";			  	   
			  	   // 20 -PRIMERA ETAPA +TOPICO 1
			  	   solicitud += state.getPrimeraEtapa();
			  	   solicitud += "\n";
			  	   // 21 -Etapa Proyecto
			  	   solicitud += etapaProyecto;
			  	   solicitud += "\n";
			  	   // 22 -Electiva o general adicional
			  	   solicitud += state.getElectivaAdicional();
			  	   solicitud += "\n";			  	   
			  	   // 23 -SIN REQUISITOS
			  	   solicitud += state.getSinRequisito();
			  	   solicitud += "\n";	
		           // 24 -Codigo de materias a pedir permiso
				   solicitud += permisos;
			  	   solicitud += "\n";
			  	   // 25 -Proyecto a Dedicacion Exclusiva
				   solicitud += state.getProyectoDedicacion();
			  	   solicitud += "\n";		  	   
		           // 26 -Observaciones
			  	   solicitud += state.getPermisoOtros();
			  	   solicitud += "\n";

			  	   state.setSolicitud(solicitud);
//		          Toast.makeText( getBaseContext(),
//		                 "Done writing SD 'Estudiante.txt'",
//		                   Toast.LENGTH_SHORT).show();
		       } catch (Exception e) {
//		           Toast.makeText( getBaseContext(), e.getMessage(),
//		                   Toast.LENGTH_SHORT).show();
		       }
		   }
		   
	   public void enviarCorreo(){
    	  Intent nextActivity = new Intent(this,SendMailActivity.class);
//          nextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//          nextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);                  
//          nextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
          startActivity(nextActivity);
	       		   
	   }
		
	   
		 public void envioCorreo(View v) {
			 
			 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

			 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
			 dlgAlert.setTitle("Mensaje de Confirmaci�n");
			 dlgAlert.setPositiveButton("Aceptar", null);
			 dlgAlert.setNegativeButton("Cancelar", null);

			 dlgAlert.setCancelable(true);


			 dlgAlert.setPositiveButton("Aceptar",
	         new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	
					LayoutInflater layoutInflater = LayoutInflater.from(context);

					View promptView = layoutInflater.inflate(R.layout.prompts, null);

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

					// set prompts.xml to be the layout file of the alertdialog builder
					alertDialogBuilder.setView(promptView);

					final EditText input = (EditText) promptView.findViewById(R.id.userInput);

					// setup a dialog window
					alertDialogBuilder
							.setCancelable(false)
							.setPositiveButton("OK", new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											// get user input and set it to result
											MyApplication state =((MyApplication) getApplicationContext());
											state.setContrasenaCorreo(input.getText().toString());
							            	escribirArchivo();
							            	enviarCorreo();
										}
									})
							.setNegativeButton("Cancel",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											dialog.cancel();
										}
									});

					// create an alert dialog
					AlertDialog alertD = alertDialogBuilder.create();

					alertD.show();				
	
	            }
			 });	
			 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	dialog.cancel();

	            }
	           
			 });
			 
			 dlgAlert.create().show();
		}

			 
/* ########################## PERMISOS ELECTIVA AREAS ####################*/	 			 

	 public void solicitarPermisoAreas(View v){

		 
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
         new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

	       		 EditText e1 		    = (EditText)findViewById(R.id.etArea1);
	    		 EditText e2 		    = (EditText)findViewById(R.id.etArea2);
	    		 EditText e3 		    = (EditText)findViewById(R.id.etArea3);
	    		 CheckBox e4 		    = (CheckBox)findViewById(R.id.adicional_electiva_area);
	    		 
	    		 MyApplication state    = (MyApplication)getApplicationContext();
	    		 ArrayList<String> area = new ArrayList<String>();
	    		 ArrayList<String> permiso = state.getPermiso(5);
	    		 
	    		 if(!e1.getText().toString().trim().equals("")){
	    			 area.add(e1.getText().toString());
	    			 permiso.add(e1.getText().toString());
	    		 }
	    		 if(!e2.getText().toString().trim().equals("")){
	    			 area.add(e2.getText().toString());
	    			 permiso.add(e2.getText().toString());			 
	    		 }
	    		 if(!e3.getText().toString().trim().equals("")){
	    			 area.add(e3.getText().toString());
	    			 permiso.add(e3.getText().toString());			 
	    		 }	
	    		 
	    		 if(e4.isChecked()){
	    			 state.setElectivaAdicional("si");
	    		 }
	    		 
	    		 state.setElectivaArea(area);
	    		// state.setPermiso(5,permiso);	
            	
            }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
		 });		 
		 
		 dlgAlert.create().show();
 
	 }	
	 
	 
/* ########################## PERMISOS ELECTIVA LIBRES ####################*/
	 public void solicitarPermisoLibres(View v){
		 
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
         new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	
          
			 EditText e1 		    = (EditText)findViewById(R.id.etLibre1);
			 EditText e2 		    = (EditText)findViewById(R.id.etLibre2);
			 EditText e3 		    = (EditText)findViewById(R.id.etLibre3);
    		 CheckBox e4 		    = (CheckBox)findViewById(R.id.electiva_libre_adicional);			 
			 MyApplication state    = (MyApplication)getApplicationContext();
			 ArrayList<String> libre= new ArrayList<String>();
			 ArrayList<String> permiso = state.getPermiso(5);		 
			 
			 if(!e1.getText().toString().trim().equals("")){
				 libre.add(e1.getText().toString());
				 permiso.add(e1.getText().toString());			 
			 }
			 if(!e2.getText().toString().trim().equals("")){
				 libre.add(e2.getText().toString());
				 permiso.add(e2.getText().toString());	
			 }
			 if(!e3.getText().toString().trim().equals("")){
				 libre.add(e3.getText().toString());
				 permiso.add(e3.getText().toString());			 
			 }	
	
    		 
    		 if(e4.isChecked()){
    			 state.setElectivaAdicional("si");
    		 }
			 
			 state.setElectivaLibre(libre);
			// state.setPermiso(5,permiso);
		 
            }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
		 });		 
		 
		 dlgAlert.create().show();
	 }	
	 
/* ########################## PERMISOS ETAPA PROYECTO ####################*/
	 public void solicitarPermisoEtapaProyecto(View v){
		 
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
         new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
		 
		 
			 RadioButton e0 		 = (RadioButton)findViewById(R.id.etapa1_proyecto_grado);
			 EditText e1 		    = (EditText)findViewById(R.id.etEtapaProyecto1);
			 EditText e2 		    = (EditText)findViewById(R.id.etEtapaProyecto2);
			 RadioButton e3 		 = (RadioButton)findViewById(R.id.etapa);
			 
			 MyApplication state    = (MyApplication)getApplicationContext();
			 ArrayList<String> etapa= new ArrayList<String>();
			 
			 
			 if(e0.isChecked()){
				 //se asigna "si" al estado de 1era etapa de proyecto y topicos I. cuyo c�digo
				 // es EP-1308 + EP-5855.
				 state.setPrimeraEtapa("si");
				 ArrayList<String> permiso = state.getPermiso(5);
				 permiso.add("EP1308");
				 permiso.add("EP5855");
			 }
			 
			 if(e3.isChecked()){
				 	 //si el usuario ingresa una etapa del proyecto.
				 if(!e1.getText().toString().trim().equals("")){
					 etapa.add(e1.getText().toString());
				 }
				 if(!e2.getText().toString().trim().equals("")){
					 etapa.add(e2.getText().toString());
				 }
			 
				 state.setEtapaProyecto(etapa);
			 
			 }
            }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
		 });		 
		 
		 dlgAlert.create().show();
	 }	

/* ########################## PERMISOS EXCESO CREDITOS ####################*/
			 
	 public void solicitarPermisoExcesoCreditos(View v){
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
         new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
		 
				 EditText e1 		 = (EditText)findViewById(R.id.etMaxCreditos);
				 MyApplication state = (MyApplication)getApplicationContext();
				 
				 if(!e1.getText().toString().trim().equals("")){
					 state.setMaxCreditos(e1.getText().toString()); 
				 } 

            }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
		 });		 
		 
		 dlgAlert.create().show();
	 }					 
				 
/* ########################## PERMISOS MINIMO CREDITOS ####################*/
	 
	 public void solicitarPermisoMinCreditos(View v){
		 
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
         new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
		 
				 EditText e1 		 = (EditText)findViewById(R.id.etMinCreditos);
				 MyApplication state = (MyApplication)getApplicationContext();
				 
				 if(!e1.getText().toString().trim().equals("")){
					 state.setMinCreditos(e1.getText().toString()); 
				 } 
            }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
		 });		 
		 
		 dlgAlert.create().show();
	 }	
	 
/* ########################## PERMISOS EXTRAPLAN ####################*/
	 
	 public void solicitarPermisoExtraplan(View v){
		 
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
         new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

				 EditText e1 		    = (EditText)findViewById(R.id.etExtraPlan01);
				 EditText e2 		    = (EditText)findViewById(R.id.etExtraPlan02);
				 EditText e3 		    = (EditText)findViewById(R.id.etExtraPlan03);
				 MyApplication state    = (MyApplication)getApplicationContext();
				 ArrayList<String> permiso = state.getPermiso(5);		
				 
				 if(!e1.getText().toString().trim().equals("")){
					 permiso.add(e1.getText().toString());
					 state.setExtraplanGeneral("si");
				 }
				 if(!e2.getText().toString().trim().equals("")){
					 permiso.add(e2.getText().toString());
					 state.setExtraplan("si");					 
				 }
				 if(!e3.getText().toString().trim().equals("")){
					 permiso.add(e3.getText().toString());
					 state.setExtraplan("si");		
				 }		 
				 
            }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
		 });		 
		 
		 dlgAlert.create().show();
	 }		 
	 
	 /* ########################## PERMISOS PASANTIA ####################*/		 
	 public void solicitarPermisoPasantia(View v){
		 
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
         new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

				 RadioButton e1 		 = (RadioButton)findViewById(R.id.cbPasantia1);
				 RadioButton e2 		 = (RadioButton)findViewById(R.id.cbPasantia2);
				 MyApplication state = (MyApplication)getApplicationContext();
				 
				 if(e2.isChecked()){
					 state.setPasantiaLarga("si");
					 ArrayList<String> permiso = state.getPermiso(5);
					 permiso.add("EP3420");
				 }
				 if(e1.isChecked()){
					 state.setPasantiaCorta("si");
					 ArrayList<String> permiso = state.getPermiso(5);
					 permiso.add("EP1420");
				 }
            }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
		 });		 
		 
		 dlgAlert.create().show();
	 }	
	 	
	 

	 
	 
/* ########################## PERMISOS SIN REQUISITO ####################*/	 
	 
	 public void solicitarPermisoSinRequisito(View v){
			
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
         new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
		 
		 /*
		  * AQUI EL CODIGO PARA VERIFICAR Y GUARDAR EN EL TXT.
		  */

				 EditText e1 		    = (EditText)findViewById(R.id.etSinRequisito);
				 MyApplication state    = (MyApplication)getApplicationContext();
				 
				 if(!e1.getText().toString().trim().equals("")){
					 state.setSinRequisito(e1.getText().toString());
				 }          	
            	
            }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
		 });		 
		 
		 dlgAlert.create().show();
	 }	
	 
	 
	 
/* ########################## PERMISOS GENERALES ####################*/	 
	 
	public void solicitarPermisoGenerales(View v){
		
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
        new DialogInterface.OnClickListener() {
           public void onClick(DialogInterface dialog, int which) {
		 
        	   RadioButton e1 		    = (RadioButton)findViewById(R.id.cbAntepenultimo);
        	   RadioButton e2 		    = (RadioButton)findViewById(R.id.cbPenultimo);
        	   RadioButton e3 		    = (RadioButton)findViewById(R.id.cbUltimo);
				 MyApplication state    = (MyApplication)getApplicationContext();
				
					
				 if(e1.isChecked() || e2.isChecked() || e3.isChecked()){
					 state.setDosGenerales("si");
				 }	

           }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
           public void onClick(DialogInterface dialog, int which) {
           	dialog.cancel();
           }
		 });		 
		 
		 dlgAlert.create().show();
	 }	 
	 

/* ########################## PERMISOS OTROS ####################*/		 
	 public void solicitarPermisoOtros(View v){
		 
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
         new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
		 
				 EditText e1 		    = (EditText)findViewById(R.id.etOtros);;
				 MyApplication state    = (MyApplication)getApplicationContext();
				 
				 if(!e1.getText().toString().trim().equals("")){
					 state.setPermisoOtros(e1.getText().toString());
				 }
            }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
		 });		 
		 
		 dlgAlert.create().show();
	 }		 
	 
	 /*########################PERMISO PROYECTO DEDICACI�N ######################## */
	 
	 
     public void solicitarPermisoPDE(View v){
   	  
		 AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		 dlgAlert.setMessage("�Est� seguro que desea realizar �sta solicitud?");
		 dlgAlert.setTitle("Mensaje de Confirmaci�n");
		 dlgAlert.setPositiveButton("Aceptar", null);
		 dlgAlert.setNegativeButton("Cancelar", null);

		 dlgAlert.setCancelable(true);


		 dlgAlert.setPositiveButton("Aceptar",
         new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
   	  
		   	  CheckBox e1 		 = (CheckBox)findViewById(R.id.checkPDE);
		   	  MyApplication state = (MyApplication)getApplicationContext();
			 
		   	  if(e1.isChecked()){
		   		  state.setProyectoDedicacion("si");
					 ArrayList<String> permiso = state.getPermiso(5);
					 permiso.add("EP5408");
		   	  }
   	  
            }
		 });	
		 dlgAlert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
		 });		 
		 
		 dlgAlert.create().show();
	 }	 
     
     
     /*
      * M�todo a ejecutar cuando se selecciona el boton "back" de android.
      * Se imprime un mensaje de confirmaci�n, y si en 3 seg el bot�n vuelve
      * a ser presionado, se sale de la aplicaci�n.  
      */
     private Boolean exit = false;
     @Override
         public void onBackPressed() {
             if (exit)
                 finish();
             else {
                 Toast.makeText(this, "Presione de nuevo para salir.",
                         Toast.LENGTH_SHORT).show();
                 exit = true;
                 new Handler().postDelayed(new Runnable() {
                     @Override
                     public void run() {
                         exit = false;
                     }
                 }, 3 * 1000);

             }

         }
	 
}
