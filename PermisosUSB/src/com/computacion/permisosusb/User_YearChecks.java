package com.computacion.permisosusb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

public class User_YearChecks extends ActionBarActivity {

	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
			setContentView(R.layout.student_year_checks);
		}
	
	public void contBtn(View view) {
	    Intent intent = new Intent(this, Internship_User_GraphFill01.class);
	    startActivity(intent);
	}
}
