package com.computacion.permisosusb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

public class Exclusive_User_GraphFill_Proyecto_Dedicacion  extends ActionBarActivity {

	private Button b;	
	private RadioButton radioPasada ;
	private RadioButton radioCursando ;
	private RadioButton radioPermiso ;
	private CheckBox seleccionarTodo;
	@SuppressWarnings("unused")
	private Button b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10;	
	
	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
			setContentView(R.layout.proyecto_dedicacion);
		}

	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.main, menu);
	 
	        return super.onCreateOptionsMenu(menu);
	    }
		
	    public boolean onOptionsItemSelected(MenuItem item) {
	    	
	    	
	          Intent nextActivity = new Intent(this,Solicitud_Permisos.class);
	          //Para que las vistas no sean empiladas, y el bot�n de back pueda cerrar la app.
	          nextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	          nextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);                  
	          nextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
	          startActivity(nextActivity);
	       
	          return super.onOptionsItemSelected(item);
	    }
	    
		public void agregarMateria(View view) {

			radioPasada = (RadioButton) findViewById(R.id.rbMateriaPasada);
			radioCursando = (RadioButton) findViewById(R.id.rbMateriaCursando);
			radioPermiso = (RadioButton) findViewById(R.id.rbMateriaPermiso);
				
			b =(Button) findViewById(view.getId());	
			if(radioPasada.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_pasada);			
			}else if (radioCursando.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_cursando);			
			}else if(radioPermiso.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_permiso);			
			}
				
		}
		
		
		public void seleccionarTodo(View view){
			
			b0 =(Button) findViewById(R.id.bProyectoDedicacion);	
			
			seleccionarTodo = (CheckBox)findViewById(R.id.select_all);
			
			if(seleccionarTodo.isChecked()){
				
				b0.setBackgroundResource(R.drawable.border_button_style_pasada);				
			}else{
				b0.setBackgroundResource(R.drawable.border_button_style);					
			}
		}
		
		
		

		
			
			
		
	}
	    