package com.computacion.permisosusb;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Permiso_DedicacionE extends Fragment {
	
	/*
	 * Fragment que muestra el permiso de Proyecto a Dedicación Exclusiva.
	 */
	
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
			   Bundle savedInstanceState) {
			  return inflater.inflate(R.layout.permiso_dedicacion_exclusiva, container,false);
			 }
}
