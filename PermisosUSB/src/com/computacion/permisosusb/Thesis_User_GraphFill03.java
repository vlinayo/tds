package com.computacion.permisosusb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

public class Thesis_User_GraphFill03  extends ActionBarActivity {

	private Button b;	
	private RadioButton radioPasada ;
	private RadioButton radioCursando ;
	private RadioButton radioPermiso ;
	private CheckBox seleccionarTodo;
	private Button b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13;	
	private ArrayList<Button> lista;
	
	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
			setContentView(R.layout.thesis_student_graph_fill_03);
		}

	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.main, menu);
	 
	        return super.onCreateOptionsMenu(menu);
	    }
		
	    public boolean onOptionsItemSelected(MenuItem item) {
	    	
	    	  MyApplication state        = ((MyApplication) getApplicationContext());
	    	  ArrayList<String> cursando = new ArrayList();
	    	  ArrayList<String> permiso  = new ArrayList();
	    	  
	    	  
				b0 =(Button) findViewById(R.id.balgortimosEstructurasTres);
				b1 =(Button) findViewById(R.id.bsistemaBasesDatosUno);
				b2 =(Button) findViewById(R.id.btraductoresInt);
				b3 =(Button) findViewById(R.id.blabAlgoritmosEstructurasTres);
				b4 =(Button) findViewById(R.id.blabBasesDatosUno);
				b5 =(Button) findViewById(R.id.bsistemaInformacionUno);
				b6 =(Button) findViewById(R.id.borganizacionComp);
				b7 =(Button) findViewById(R.id.bsistemaOperacionUno);
				b8 =(Button) findViewById(R.id.bingSoftwareUno);
				b9 =(Button) findViewById(R.id.bProbabilidad);
				b10 =(Button) findViewById(R.id.bEstadistica);
				b11 =(Button) findViewById(R.id.bEstudiosGenerales);	
				//b12 =(Button) findViewById(R.id.bServicioComunitario);
				b13 =(Button) findViewById(R.id.bPasantiaCorta);	
		  		
		  		lista = new ArrayList<Button>(Arrays.asList(b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b13));
		  		Iterator i = lista.iterator();
		  		while(i.hasNext()){
	    		  b = (Button)i.next();
	    		  if(b.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.border_button_style_cursando).getConstantState())) { 
	    			 cursando.add(b.getHint().toString());
	    		  }else if(b.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.border_button_style_permiso).getConstantState())){
	     			 permiso.add(b.getHint().toString());	
	    		  }  
	    	  }
	    	  
	    	  state.setCursando(2, cursando);
	    	  state.setPermiso(2, permiso);

	    	
	    	
	    	
	          Intent nextActivity = new Intent(this,Thesis_User_GraphFill04.class);
	          startActivity(nextActivity);
	       
	        return super.onOptionsItemSelected(item);
	    }
	    
	    
		public void agregarMateria(View view) {

			radioPasada = (RadioButton) findViewById(R.id.rbMateriaPasada);
			radioCursando = (RadioButton) findViewById(R.id.rbMateriaCursando);
			radioPermiso = (RadioButton) findViewById(R.id.rbMateriaPermiso);
				
			b =(Button) findViewById(view.getId());	
			if(radioPasada.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_pasada);			
			}else if (radioCursando.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_cursando);			
			}else if(radioPermiso.isChecked()){
				b.setBackgroundResource(R.drawable.border_button_style_permiso);			
			}
				
		}
		
		
		public void seleccionarTodo(View view){
			
			b0 =(Button) findViewById(R.id.balgortimosEstructurasTres);
			b1 =(Button) findViewById(R.id.bsistemaBasesDatosUno);
			b2 =(Button) findViewById(R.id.btraductoresInt);
			b3 =(Button) findViewById(R.id.blabAlgoritmosEstructurasTres);
			b4 =(Button) findViewById(R.id.blabBasesDatosUno);
			b5 =(Button) findViewById(R.id.bsistemaInformacionUno);
			b6 =(Button) findViewById(R.id.borganizacionComp);
			b7 =(Button) findViewById(R.id.bsistemaOperacionUno);
			b8 =(Button) findViewById(R.id.bingSoftwareUno);
			b9 =(Button) findViewById(R.id.bProbabilidad);
			b10 =(Button) findViewById(R.id.bEstadistica);
			b11 =(Button) findViewById(R.id.bEstudiosGenerales);	
			//b12 =(Button) findViewById(R.id.bServicioComunitario);
			b13 =(Button) findViewById(R.id.bPasantiaCorta);				
			
			seleccionarTodo = (CheckBox)findViewById(R.id.select_all);
			
			if(seleccionarTodo.isChecked()){
				
				b0.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b1.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b2.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b3.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b4.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b5.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b6.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b7.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b8.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b9.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b10.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b11.setBackgroundResource(R.drawable.border_button_style_pasada);
				//b12.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b13.setBackgroundResource(R.drawable.border_button_style_pasada);					
			}else{
				b0.setBackgroundResource(R.drawable.border_button_style);		
				b1.setBackgroundResource(R.drawable.border_button_style);		
				b2.setBackgroundResource(R.drawable.border_button_style);		
				b3.setBackgroundResource(R.drawable.border_button_style);		
				b4.setBackgroundResource(R.drawable.border_button_style);		
				b5.setBackgroundResource(R.drawable.border_button_style);		
				b6.setBackgroundResource(R.drawable.border_button_style);		
				b7.setBackgroundResource(R.drawable.border_button_style);		
				b8.setBackgroundResource(R.drawable.border_button_style);		
				b9.setBackgroundResource(R.drawable.border_button_style);		
				b10.setBackgroundResource(R.drawable.border_button_style);		
				b11.setBackgroundResource(R.drawable.border_button_style);
				//b12.setBackgroundResource(R.drawable.border_button_style);		
				b13.setBackgroundResource(R.drawable.border_button_style);					
			}
		}
		
		
		
		public void seleccionarSD(View view){
			
			b0 =(Button) findViewById(R.id.balgortimosEstructurasTres);
			b3 =(Button) findViewById(R.id.blabAlgoritmosEstructurasTres);
			b6 =(Button) findViewById(R.id.borganizacionComp);
			b9 =(Button) findViewById(R.id.bProbabilidad);
			b11 =(Button) findViewById(R.id.bEstudiosGenerales);
			seleccionarTodo = (CheckBox)findViewById(R.id.trimester_S_D);
			
			if(seleccionarTodo.isChecked()){
				
				b0.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b3.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b6.setBackgroundResource(R.drawable.border_button_style_pasada);
				b9.setBackgroundResource(R.drawable.border_button_style_pasada);				
				b11.setBackgroundResource(R.drawable.border_button_style_pasada);				
			}else{
				b0.setBackgroundResource(R.drawable.border_button_style);	
				b3.setBackgroundResource(R.drawable.border_button_style);		
				b6.setBackgroundResource(R.drawable.border_button_style);	
				b9.setBackgroundResource(R.drawable.border_button_style);					
				b11.setBackgroundResource(R.drawable.border_button_style);				
			}
		}
		
		
		public void seleccionarEM(View view){
			
			b1 =(Button) findViewById(R.id.bsistemaBasesDatosUno);
			b4 =(Button) findViewById(R.id.blabBasesDatosUno);
			b7 =(Button) findViewById(R.id.bsistemaOperacionUno);
			b10 =(Button) findViewById(R.id.bEstadistica);	
			b11 =(Button) findViewById(R.id.bServicioComunitario);			
			seleccionarTodo = (CheckBox)findViewById(R.id.trimester_E_M);
			
			if(seleccionarTodo.isChecked()){
						
				b1.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b4.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b7.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b10.setBackgroundResource(R.drawable.border_button_style_pasada);
				b11.setBackgroundResource(R.drawable.border_button_style_pasada);				
			}else{		
				b1.setBackgroundResource(R.drawable.border_button_style);		
				b4.setBackgroundResource(R.drawable.border_button_style);		
				b7.setBackgroundResource(R.drawable.border_button_style);	
				b10.setBackgroundResource(R.drawable.border_button_style);		
				b11.setBackgroundResource(R.drawable.border_button_style);					
			}

		}
		
		public void seleccionarAJ(View view){
			
			b2 =(Button) findViewById(R.id.btraductoresInt);
			b5 =(Button) findViewById(R.id.bsistemaInformacionUno);
			b8 =(Button) findViewById(R.id.bingSoftwareUno);	
			b12 =(Button) findViewById(R.id.bPasantiaCorta);				
			seleccionarTodo = (CheckBox)findViewById(R.id.trimester_A_J);
			
			if(seleccionarTodo.isChecked()){
						
				b2.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b5.setBackgroundResource(R.drawable.border_button_style_pasada);		
				b8.setBackgroundResource(R.drawable.border_button_style_pasada);
				b12.setBackgroundResource(R.drawable.border_button_style_pasada);					
			}else{
				
				b2.setBackgroundResource(R.drawable.border_button_style);		
				b5.setBackgroundResource(R.drawable.border_button_style);		
				b8.setBackgroundResource(R.drawable.border_button_style);
				b12.setBackgroundResource(R.drawable.border_button_style);				
			}
		}	
			
		
	}
